// An example configuration file.
exports.config = {
  directConnect: true,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',
  //onPrepare: function() {
          //var jasmineReporters = require('C:/Users/RE041943/Desktop/guru/node_modules/jasmine-reporters');
          //jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter(null, true, true)
          //);

  // Spec patterns are relative to the current working directory when
  // protractor is called.
  specs: ['spec.js'],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  },

  onPrepare: function(){
    browser.manage().window().maximize();
  },

  params: {url: 'https://www.etsy.com/'}
};
