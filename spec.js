
  var helper = require('./helper');

  describe('Main Page: login/register',function(){
    beforeEach(function () {
      browser.waitForAngularEnabled(false);
      browser.get(browser.params.url);

    });
    afterEach(function () {
       browser.manage().deleteAllCookies();

    });
   it ('should have something', function(){
      //browser.driver.manage().window().maximize();

      var page_title = 'Etsy.com | Shop for anything from creative people everywhere'
       expect(browser.getTitle()).toEqual(page_title);

   });
   it ('should sign in and verify error apper', function(){
      //variables
      var signInbtn = element(by.id('sign-in'));
      var signInSubmitBtn = element(by.id('signin-button'));
      var userNameField = element(by.id('username-existing'));
      var userPasswordField = element(by.id('password-existing'));
      var password_Existing_error = element(by.css('.msg-error'));
      var password_Existing_error_text = 'Please reset your password to sign into Etsy. We’re taking this security measure to protect your account.\
    Get tips on making your account more secure. If you have any trouble, contact Etsy Support.'

      //actions
      signInbtn.click();
      helper.waitUntilReady(userNameField);
      //userNameField.clear();
      userNameField.sendKeys('test@test.com');
      helper.waitUntilReady(userPasswordField);
      userPasswordField.sendKeys('password');
      signInSubmitBtn.click();
      helper.waitUntilReady(password_Existing_error);
      expect(password_Existing_error.getText()).toEqual(password_Existing_error_text)

   });

   it('should register a new user', function() {
       var signInbtn = element(by.id('sign-in'));
       var regButtonOnMainPage = element(by.id('register'));
       var firstName = element(by.id('first-name'))
       var emailField = element(by.id('email'));
       var passwordField = element(by.name('password'));
       var passwordConfirm = element(by.id('password-repeat'));
       var password = 'Password';
       var registerButton = element(by.id('register_button'));

       regButtonOnMainPage.click();
       helper.waitUntilReady(firstName);
       firstName.sendKeys('Leon');
       emailField.sendKeys('test@test.com');
       passwordField.sendKeys(password); //*[@id="inline-overlay"]
       passwordConfirm.sendKeys(password);
       registerButton.click();


   });



  });
